package ru.t1.kravtsov.tm.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.endpoint.IUserEndpoint;
import ru.t1.kravtsov.tm.api.service.IAuthService;
import ru.t1.kravtsov.tm.api.service.IServiceLocator;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.model.Session;
import ru.t1.kravtsov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService(endpointInterface = "ru.t1.kravtsov.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(final @NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/changeUserPassword")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserChangePasswordResponse changeUserPassword(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String password = request.getPassword();
        @Nullable final User user = getServiceLocator().getUserService().setPassword(userId, password);
        return new UserChangePasswordResponse(user);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/lockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserLockResponse lockUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/registryUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserRegistryResponse registryUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        check(request);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @NotNull final IAuthService authService = getServiceLocator().getAuthService();
        @NotNull final User user = authService.registry(login, password, email);
        return new UserRegistryResponse(user);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/removeUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserRemoveResponse removeUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @NotNull final User user = getServiceLocator().getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/unlockUser")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserUnlockResponse unlockUser(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getServiceLocator().getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @POST
    @NotNull
    @Override
    @WebMethod
    @Path("/updateUserProfile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public UserUpdateProfileResponse updateUserProfile(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        if (firstName == null || lastName == null || middleName == null) {
            return new UserUpdateProfileResponse();
        }
        @NotNull final User user = getServiceLocator().getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

}
