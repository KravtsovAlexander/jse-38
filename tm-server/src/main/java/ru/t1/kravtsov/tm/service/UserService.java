package ru.t1.kravtsov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.api.service.*;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.exception.entity.UserNotFoundException;
import ru.t1.kravtsov.tm.exception.field.*;
import ru.t1.kravtsov.tm.model.User;
import ru.t1.kravtsov.tm.repository.UserRepository;
import ru.t1.kravtsov.tm.util.HashUtil;

import java.sql.Connection;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull IConnectionService connectionSerivce,
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionSerivce);
        this.projectService = projectService;
        this.taskService = taskService;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(final @NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        @NotNull User addedUser;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            addedUser = repository.add(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return addedUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (doesEmailExist(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        user.setEmail(email);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        @NotNull User addedUser;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            addedUser = repository.add(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return addedUser;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (doesLoginExist(login)) throw new LoginExistsException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        @NotNull User addedUser;
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            addedUser = repository.add(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return addedUser;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        if (login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByLogin(login);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        if (email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.findByEmail(email);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean doesLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.doesLoginExist(login);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean doesEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        try (@NotNull final Connection connection = getConnection()) {
            @NotNull final IUserRepository repository = getRepository(connection);
            return repository.doesEmailExist(email);
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        return remove(user);
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = findByEmail(email);
        return remove(user);
    }

    @NotNull
    @Override
    public User remove(@Nullable final User model) {
        if (model == null) throw new UserNotFoundException();
        @Nullable final User user = super.remove(model);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskService.deleteAll(userId);
        projectService.deleteAll(userId);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User setPassword(@Nullable String id, @Nullable String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = findOneById(id);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User updateUser(@Nullable String id, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final User user = findOneById(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        @NotNull final Connection connection = connectionSerivce.getConnection();
        try {
            @NotNull final IUserRepository repository = getRepository(connection);
            repository.update(user);
            connection.commit();
        } catch (@NotNull Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

}
