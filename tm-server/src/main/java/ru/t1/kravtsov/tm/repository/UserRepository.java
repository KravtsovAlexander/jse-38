package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserRepository;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @NotNull
    private final String COLUMN_LOGIN = "login";

    @NotNull
    private final String COLUMN_LOCKED = "locked";

    @NotNull
    private final String COLUMN_LAST_NAME = "last_name";

    @NotNull
    private final String COLUMN_MIDDLE_NAME = "middle_name";

    @NotNull
    private final String COLUMN_FIRST_NAME = "first_name";

    @NotNull
    private final String COLUMN_EMAIL = "email";

    @NotNull
    private final String COLUMN_ROLE = "role";

    @NotNull
    private final String COLUMN_PASSWORD_HASH = "password_hash";

    public UserRepository(final @NotNull Connection connection) {
        super(connection);
    }

    @Override
    protected @NotNull String getTableName() {
        return "tm_user";
    }

    @Override
    @SneakyThrows
    protected @NotNull User fetch(final @NotNull ResultSet row) {
        @NotNull final User user = new User();
        user.setId(row.getString(COLUMN_ID));
        user.setLogin(row.getString(COLUMN_LOGIN));
        user.setLocked(row.getBoolean(COLUMN_LOCKED));
        user.setLastName(row.getString(COLUMN_LAST_NAME));
        user.setMiddleName(row.getString(COLUMN_MIDDLE_NAME));
        user.setFirstName(row.getString(COLUMN_FIRST_NAME));
        user.setEmail(row.getString(COLUMN_EMAIL));
        user.setRole(Role.valueOf(row.getString(COLUMN_ROLE)));
        user.setPasswordHash(row.getString(COLUMN_PASSWORD_HASH));
        return user;
    }

    @Override
    @SneakyThrows
    public @NotNull User add(final @NotNull User model) {
        @NotNull final String sql = "INSERT INTO " + getTableName()
                + " ("
                + COLUMN_ID + ", "
                + COLUMN_LOGIN + ", "
                + COLUMN_LOCKED + ", "
                + COLUMN_LAST_NAME + ", "
                + COLUMN_MIDDLE_NAME + ", "
                + COLUMN_FIRST_NAME + ", "
                + COLUMN_EMAIL + ", "
                + COLUMN_ROLE + ", "
                + COLUMN_PASSWORD_HASH
                + ") "
                + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getLogin());
            statement.setBoolean(3, model.isLocked());
            statement.setString(4, model.getLastName());
            statement.setString(5, model.getMiddleName());
            statement.setString(6, model.getFirstName());
            statement.setString(7, model.getEmail());
            statement.setString(8, String.valueOf(model.getRole()));
            statement.setString(9, model.getPasswordHash());
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(final @NotNull User model) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET "
                + COLUMN_LOGIN + " = ?, "
                + COLUMN_LOCKED + " = ?, "
                + COLUMN_LAST_NAME + " = ?, "
                + COLUMN_MIDDLE_NAME + " = ?, "
                + COLUMN_FIRST_NAME + " = ?, "
                + COLUMN_EMAIL + " = ?, "
                + COLUMN_ROLE + " = ?, "
                + COLUMN_PASSWORD_HASH + " = ? "
                + " WHERE " + COLUMN_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getLogin());
            statement.setBoolean(2, model.isLocked());
            statement.setString(3, model.getLastName());
            statement.setString(4, model.getMiddleName());
            statement.setString(5, model.getFirstName());
            statement.setString(6, model.getEmail());
            statement.setString(7, String.valueOf(model.getRole()));
            statement.setString(8, model.getPasswordHash());
            statement.setString(9, model.getId());
            statement.executeUpdate();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@NotNull final String login) {
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_LOGIN + " = ? ";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet row = statement.executeQuery();
            if (!row.next()) return null;
            return fetch(row);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@NotNull final String email) {
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_EMAIL + " = ? ";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet row = statement.executeQuery();
            if (!row.next()) return null;
            return fetch(row);
        }
    }

    @NotNull
    @Override
    public Boolean doesLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean doesEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
