package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IRepository;
import ru.t1.kravtsov.tm.comparator.CreatedComparator;
import ru.t1.kravtsov.tm.comparator.StatusComparator;
import ru.t1.kravtsov.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final String COLUMN_CREATED = "created";

    @NotNull
    protected final String COLUMN_DESCRIPTION = "description";

    @NotNull
    protected final String COLUMN_ID = "id";

    @NotNull
    protected final String COLUMN_NAME = "name";

    @NotNull
    protected final String COLUMN_STATUS = "status";

    @NotNull
    protected final Connection connection;

    @NotNull
    protected abstract String getTableName();

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected Predicate<M> filterById(@NotNull final String id) {
        return m -> id.equals(m.getId());
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String sql = "DELETE FROM " + getTableName();
        try (@NotNull final Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql);
        }
    }

    @Override
    public void deleteAll() {
        clear();
    }

    @Override
    public void deleteAll(@NotNull final List<M> models) {
        for (M model : models) {
            remove(model);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName();
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet row = statement.executeQuery(sql);
            while (row.next()) {
                result.add(fetch(row));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql;
        if (comparator != null) {
            sql = "SELECT * FROM " + getTableName() + " ORDER BY " + getSortType(comparator);
        } else {
            sql = "SELECT * FROM " + getTableName();
        }
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet row = statement.executeQuery(sql);
            while (row.next()) {
                result.add(fetch(row));
            }
        }
        return result;
    }

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator.equals(CreatedComparator.INSTANCE)) return COLUMN_CREATED;
        if (comparator.equals(StatusComparator.INSTANCE)) return COLUMN_STATUS;
        return COLUMN_NAME;
    }

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row);

    protected abstract void update(@NotNull final M model);

    @NotNull
    @Override
    public abstract M add(@NotNull final M model);

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @NotNull
    @Override

    public Collection<M> set(@NotNull final Collection<M> models) {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@NotNull final String id) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE " + COLUMN_ID + " = ? LIMIT 1";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@NotNull final Integer index) {
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " LIMIT 1 OFFSET ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setInt(1, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final String sql = "SELECT COUNT(*) FROM " + getTableName();
        try (@NotNull final Statement statement = connection.createStatement()) {
            @NotNull final ResultSet resultSet = statement.executeQuery(sql);
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final M model) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE " + COLUMN_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.executeUpdate();
        }
        return model;
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) {
        @Nullable final M model = findOneById(id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@NotNull final Integer index) {
        @Nullable final M model = findOneByIndex(index);
        if (model == null) return null;
        return remove(model);
    }

}
