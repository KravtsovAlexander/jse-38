package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IUserOwnedRepository;
import ru.t1.kravtsov.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    protected final String COLUMN_USER_ID = "user_id";

    public AbstractUserOwnedRepository(final @NotNull Connection connection) {
        super(connection);
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final String sql = "DELETE FROM " + getTableName() + " WHERE " + COLUMN_USER_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.executeUpdate();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName() + " WHERE " + COLUMN_USER_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet row = statement.executeQuery();
            while (row.next()) {
                result.add(fetch(row));
            }
        }
        return result;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        @NotNull final List<M> result = new ArrayList<>();
        @NotNull String sql;
        if (comparator != null) {
            sql = "SELECT * FROM " + getTableName() + " WHERE " + COLUMN_USER_ID + " = ? ORDER BY " + getSortType(comparator);
        } else {
            sql = "SELECT * FROM " + getTableName() + " WHERE " + COLUMN_USER_ID + " = ?";
        }
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet row = statement.executeQuery();
            while (row.next()) {
                result.add(fetch(row));
            }
        }
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_ID + " = ? AND " + COLUMN_USER_ID + " = ? "
                + "LIMIT 1";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_USER_ID + " = ? "
                + "LIMIT 1 OFFSET ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setInt(2, index);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null) return 0;
        @NotNull final String sql = "SELECT COUNT(*) FROM " + getTableName() + " WHERE " + COLUMN_USER_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            return resultSet.getInt("count");
        }
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || id == null) return null;
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || index == null) return null;
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return remove(model);
    }

    @Nullable
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        model.setUserId(userId);
        return add(model);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || model == null) return null;
        @NotNull final String sql = "DELETE FROM " + getTableName()
                + " WHERE " + COLUMN_ID + " = ? AND " + COLUMN_USER_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, userId);
            statement.executeUpdate();
            return model;
        }
    }

    @Override
    public void deleteAll(@Nullable final String userId) {
        if (userId == null) return;
        clear(userId);
    }

}
