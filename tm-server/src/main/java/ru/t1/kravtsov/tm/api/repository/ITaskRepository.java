package ru.t1.kravtsov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> removeByName(@NotNull String userId, @NotNull String name);

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    void update(@NotNull Task task);

}
