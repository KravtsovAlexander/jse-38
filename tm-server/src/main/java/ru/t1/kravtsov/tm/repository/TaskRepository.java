package ru.t1.kravtsov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.api.repository.ITaskRepository;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private final String COLUMN_PROJECT_ID = "project_id";

    public TaskRepository(final @NotNull Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return "tm_task";
    }

    @NotNull
    @Override
    @SneakyThrows
    protected Task fetch(final @NotNull ResultSet row) {
        @NotNull Task task = new Task();
        task.setId(row.getString(COLUMN_ID));
        task.setName(row.getString(COLUMN_NAME));
        task.setDescription(row.getString(COLUMN_DESCRIPTION));
        task.setCreated(row.getTimestamp(COLUMN_CREATED));
        task.setUserId(row.getString(COLUMN_USER_ID));
        task.setProjectId(row.getString(COLUMN_PROJECT_ID));
        task.setStatus(Status.valueOf(row.getString(COLUMN_STATUS)));
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Task add(final @NotNull Task model) {
        @NotNull final String sql = "INSERT INTO " + getTableName()
                + " ("
                + COLUMN_ID + ", "
                + COLUMN_NAME + ", "
                + COLUMN_DESCRIPTION + ", "
                + COLUMN_CREATED + ", "
                + COLUMN_USER_ID + ", "
                + COLUMN_PROJECT_ID + ", "
                + COLUMN_STATUS
                + ") "
                + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getId());
            statement.setString(2, model.getName());
            statement.setString(3, model.getDescription());
            statement.setTimestamp(4, new Timestamp(model.getCreated().getTime()));
            statement.setString(5, model.getUserId());
            statement.setString(6, model.getProjectId());
            statement.setString(7, String.valueOf(model.getStatus()));
            statement.executeUpdate();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void update(final @NotNull Task model) {
        @NotNull final String sql = "UPDATE " + getTableName() + " SET "
                + COLUMN_NAME + " = ?, "
                + COLUMN_DESCRIPTION + " = ?, "
                + COLUMN_CREATED + " = ?, "
                + COLUMN_USER_ID + " = ?, "
                + COLUMN_PROJECT_ID + " = ?, "
                + COLUMN_STATUS + " = ? "
                + " WHERE " + COLUMN_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, model.getName());
            statement.setString(2, model.getDescription());
            statement.setTimestamp(3, new Timestamp(model.getCreated().getTime()));
            statement.setString(4, model.getUserId());
            statement.setString(5, model.getProjectId());
            statement.setString(6, String.valueOf(model.getStatus()));
            statement.setString(7, model.getId());
            statement.executeUpdate();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<Task> removedProjects = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_USER_ID + " = ? "
                + "AND " + COLUMN_NAME + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, name);
            @NotNull final ResultSet row = statement.executeQuery();
            while (row.next()) {
                @NotNull final Task task = fetch(row);
                removedProjects.add(remove(task));
            }
        }
        return removedProjects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final List<Task> tasks = new ArrayList<>();
        @NotNull final String sql = "SELECT * FROM " + getTableName()
                + " WHERE " + COLUMN_USER_ID + " = ? "
                + "AND " + COLUMN_PROJECT_ID + " = ?";
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet row = statement.executeQuery();
            while (row.next()) {
                tasks.add(fetch(row));
            }
        }
        return tasks;
    }

}
