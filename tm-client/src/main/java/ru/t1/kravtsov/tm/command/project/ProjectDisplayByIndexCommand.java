package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.ProjectDisplayByIndexRequest;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Display project by index.";

    @NotNull
    public static final String NAME = "project-display-by-index";

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer input = TerminalUtil.nextNumber();
        @NotNull final Integer index = input - 1;
        @NotNull final ProjectDisplayByIndexRequest request = new ProjectDisplayByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final Project project = getProjectEndpoint()
                .displayProjectByIndex(request)
                .getProject();
        displayProject(project);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
