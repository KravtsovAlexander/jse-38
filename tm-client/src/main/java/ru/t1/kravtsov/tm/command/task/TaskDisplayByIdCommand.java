package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.dto.request.TaskDisplayByIdRequest;
import ru.t1.kravtsov.tm.model.Task;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskDisplayByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @NotNull
    public static final String NAME = "task-display-by-id";

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDisplayByIdRequest request = new TaskDisplayByIdRequest(getToken());
        request.setId(id);
        @Nullable final Task task = getTaskEndpoint()
                .displayTaskById(request)
                .getTask();
        displayTask(task);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
