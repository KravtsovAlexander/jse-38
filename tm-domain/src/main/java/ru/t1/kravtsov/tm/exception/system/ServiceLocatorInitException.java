package ru.t1.kravtsov.tm.exception.system;

public final class ServiceLocatorInitException extends AbstractSystemException {

    public ServiceLocatorInitException() {
        super("Error. Service Locator is not initialized.");
    }

}
