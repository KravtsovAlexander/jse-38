package ru.t1.kravtsov.tm.api.endpoint;

import io.swagger.annotations.ApiParam;
import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.dto.request.*;
import ru.t1.kravtsov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@WebService
@Path("/api/TaskEndpoint")
public interface ITaskEndpoint extends IEndpoint {

    @NotNull
    String NAME = "TaskEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @POST
    @NotNull
    @WebMethod
    @Path("/bindTaskToProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskBindToProjectResponse bindTaskToProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskBindToProjectRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/changeTaskStatusById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskChangeStatusByIdResponse changeTaskStatusById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/changeTaskStatusByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskChangeStatusByIndexResponse changeTaskStatusByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskChangeStatusByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/clearTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskClearResponse clearTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskClearRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/completeTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskCompleteByIdResponse completeTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/completeTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskCompleteByIndexResponse completeTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCompleteByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/createTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskCreateResponse createTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskCreateRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/displayTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskDisplayByIdResponse displayTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/displayTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskDisplayByIndexResponse displayTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskDisplayByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/listTaskByProjectId")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskListByProjectIdResponse listTaskByProjectId(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListByProjectIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/listTask")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskListResponse listTask(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskListRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskRemoveByIdResponse removeTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskRemoveByIndexResponse removeTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/removeTaskByName")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskRemoveByNameResponse removeTaskByName(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskRemoveByNameRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/startTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskStartByIdResponse startTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/startTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskStartByIndexResponse startTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskStartByIndexRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/unbindTaskFromProject")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskUnbindFromProjectResponse unbindTaskFromProject(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUnbindFromProjectRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/updateTaskById")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskUpdateByIdResponse updateTaskById(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIdRequest request
    );

    @POST
    @NotNull
    @WebMethod
    @Path("/updateTaskByIndex")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    TaskUpdateByIndexResponse updateTaskByIndex(
            @ApiParam(required = true)
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull TaskUpdateByIndexRequest request
    );

}
