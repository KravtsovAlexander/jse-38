package ru.t1.kravtsov.tm.dto.request;

import io.swagger.annotations.ApiModel;
import org.jetbrains.annotations.Nullable;

@ApiModel
public class TaskClearRequest extends AbstractUserRequest {

    public TaskClearRequest(final @Nullable String token) {
        super(token);
    }

}
